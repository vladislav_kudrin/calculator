package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * Controls sample.fxml.
 *
 * @author Vladislav
 * @version 1.1.1
 * @since 06/06/2020
 */
public class Controller {
    @FXML
    private final String infinity = String.valueOf('\u221e');

    @FXML
    private double result = 0;

    @FXML
    private String operator = "";

    @FXML
    private boolean setPoint = false;

    @FXML
    private Label label;

    /**
     * Clears all fields.
     */
    @FXML
    void clear() {
        result = 0;
        operator = "";
        setPoint = false;
        label.setText("0");
    }

    /**
     * Checks the point's status.
     */
    @FXML
    void checkPoint() {
        if(!setPoint) {
            setPoint = true;
            label.setText((label.getText().equals(infinity)) ? "0." : label.getText() + ".");
        }
    }

    /**
     * Reads a clicked number button's text and input this to {@code label}.
     *
     * @param event a clicked number button.
     */
    @FXML
    void inputNumber(ActionEvent event) {
        String number = ((Button)event.getSource()).getText();

        label.setText((label.getText().equals("0") || label.getText().equals(infinity)) ? number : label.getText() + number);
    }

    /**
     * Reads a clicked operator button's text, sets a new value to {@code result} and saves the operator button's text to {@code operator}.
     *
     * @param event a clicked operator button.
     */
    @FXML
    void checkOperator(ActionEvent event) {
        if(label.getText().equals(infinity)) return;

        String button = ((Button)event.getSource()).getText();

        result = (operator.isEmpty()) ? Double.parseDouble(label.getText()) : calculate();

        label.setText((label.getText().equals(infinity)) ? infinity : "0");
        operator = button;
        setPoint = false;
    }

    /**
     * Sets {@code result} value to {@code label}.
     */
    @FXML
    void showResult() {
        if(operator.isEmpty() || label.getText().equals(infinity)) return;

        result = calculate();
        label.setText((label.getText().equals(infinity)) ? infinity : checkNumber());
        result = 0;
        operator = "";
    }

    /**
     * Checks {@code operator} value, calls a method by {@code operator} value and returns a calculation's result.
     *
     * @return a calculation's result.
     */
    @FXML
    private double calculate() {
        switch(operator) {
            case "-":
                return subtract();
            case "*":
                return multiply();
            case "/":
                return divide();
            default:
                return add();
        }
    }

    /**
     * Sums {@code label} value to {@code result} value and returns a summarize's result.
     *
     * @return a summarize's result.
     */
    @FXML
    private double add() {
        return result + Double.parseDouble(label.getText());
    }

    /**
     * Subtracts {@code label} value from {@code result} value and returns a subtract's result.
     *
     * @return a subtract's result.
     */
    @FXML
    private double subtract() {
        return result - Double.parseDouble(label.getText());
    }

    /**
     * Multiplies {@code result} value by {@code label} value and returns a multiply's result.
     *
     * @return a multiply's result.
     */
    @FXML
    private double multiply() {
        return result * Double.parseDouble(label.getText());
    }

    /**
     * Divides {@code result} value by {@code label} value and returns a divide's result.
     * If {@code label} value is 0 returns 0 and sets to {@code label} {@code infinity} value.
     *
     * @return a divide's result
     */
    @FXML
    private double divide() {
        if(Double.parseDouble(label.getText()) == 0) {
            label.setText(infinity);
            return 0;
        }

        return result / Double.parseDouble(label.getText());
    }

    /**
     * Checks {@code result} type, sets {@code setPoint} value by determined type and returns {@code result} of determined type.
     *
     * @return {@code result} of determined type.
     */
    @FXML
    private String checkNumber() {
        long longResult = (long) Math.ceil(result);

        if(longResult == result) {
            setPoint = false;
            return String.valueOf(longResult);
        }

        setPoint = true;
        return String.valueOf(result);
    }
}